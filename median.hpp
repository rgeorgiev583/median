#include <queue>

template <typename T> class median_calculator {
  static_assert(std::is_arithmetic<T>::value, "arithmetic type required");

  std::priority_queue<T> max_heap;
  std::priority_queue<T, std::vector<T>, std::greater<T>> min_heap;

public:
  void add(T value);
  T calculate() const;
};

template <typename T> void median_calculator<T>::add(T value) {
  min_heap.push(value);
  max_heap.push(min_heap.top());
  min_heap.pop();
  if (min_heap.size() < max_heap.size()) {
    min_heap.push(max_heap.top());
    max_heap.pop();
  }
}

template <typename T> T median_calculator<T>::calculate() const {
  return min_heap.size() > max_heap.size()
             ? min_heap.top()
             : (min_heap.top() + max_heap.top()) / 2;
}