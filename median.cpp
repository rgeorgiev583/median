#include <iostream>

#include "median.hpp"

int main() {
  using namespace std;

  median_calculator<int> median;
  while (!cin.eof()) {
    int number;
    cin >> number;
    median.add(number);
  }
  cout << median.calculate();

  return 0;
}