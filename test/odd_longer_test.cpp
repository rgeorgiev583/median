#include <cassert>

#include "median.hpp"

int main() {
  median_calculator<double> median;
  for (auto i = 1; i < 1000000; i++) {
    median.add(i);
  }
  assert(median.calculate() == 500000);

  return 0;
}