#include <cmath>
#include <limits>
#include <type_traits>

template <typename T> bool almost_equal(T x, T y, int ulp) {
  static_assert(std::is_floating_point<T>::value,
                "floating-point type required");

  // the machine epsilon has to be scaled to the magnitude of the values used
  // and multiplied by the desired precision in ULPs (units in the last place)
  return std::abs(x - y) <=
             std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
         // unless the result is subnormal
         || std::abs(x - y) < std::numeric_limits<T>::min();
}