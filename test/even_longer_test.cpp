#include <cassert>

#include "compare.hpp"
#include "median.hpp"

int main() {
  median_calculator<double> median;
  for (auto i = 1; i <= 1000000; i++) {
    median.add(i);
  }
  assert(almost_equal(median.calculate(), 500000.5, 1));

  return 0;
}