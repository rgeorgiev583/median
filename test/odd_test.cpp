#include <cassert>

#include "median.hpp"

int main() {
  {
    median_calculator<int> median;
    for (auto number : {1}) {
      median.add(number);
    }
    assert(median.calculate() == 1);

    for (auto number : {5, 10, 12, 2}) {
      median.add(number);
    }
    assert(median.calculate() == 5);
  }

  {
    median_calculator<int> median;
    for (auto number : {1, 3, 3, 6, 7, 8, 9}) {
      median.add(number);
    }
    assert(median.calculate() == 6);
  }

  {
    median_calculator<int> median;
    for (auto i = 1; i < 1000; i++) {
      median.add(i);
    }
    assert(median.calculate() == 500);
  }

  return 0;
}