#include <cassert>

#include "compare.hpp"
#include "median.hpp"

int main() {
  {
    median_calculator<double> median;
    for (auto number : {1, 5, 10, 12, 2, 3, 8, 9}) {
      median.add(number);
    }
    assert(almost_equal(median.calculate(), 6.5, 1));
  }

  {
    median_calculator<double> median;
    for (auto number : {1, 2, 3, 4, 5, 6, 8, 9}) {
      median.add(number);
    }
    assert(almost_equal(median.calculate(), 4.5, 1));
  }

  {
    median_calculator<double> median;
    for (auto i = 1; i <= 1000; i++) {
      median.add(i);
    }
    assert(almost_equal(median.calculate(), 500.5, 1));
  }

  return 0;
}