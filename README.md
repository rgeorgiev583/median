# median

This project implements efficient calculation of the median of a stream of values and consists of the following components:
* a header-only library (`median.hpp`) consisting of the `median_calculator` template C++ class where the calculation of the median is implemented;
* a sample program (`median.cpp`) where the usage of the `median_calculator` class is demonstrated.  It accepts a sequence of integers on its standard input and calculates their median;
* a number of test programs, the sources of which are located in the `test` directory.

## Building

This project uses CMake, so you can use the following commands to build it:

    $ mkdir build
    $ cd build
    $ cmake ..
    $ cmake --build .

## Testing

Use the following commands to run the automated tests:

    $ cd build
    $ ctest